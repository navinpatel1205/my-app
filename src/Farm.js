
import React from 'react'
import './Farm.css';


class Farm extends React.Component {
     
  constructor(props) {
    super(props);
    this.state = {
      hidden: true,
       username: '',
    gender:'--',
    age:'--' ,
    bdate:'--',
    Address:'--',  
    state:'--',
    lists1:["Male","female","Other"],
    lists2:["AP|Andhra Pradesh",
     " AR|Arunachal Pradesh",
      "AS|Assam",
      "BR|Bihar",
     " CT|Chhattisgarh",
      "GA|Goa",
      "GJ|Gujarat",
      "HR|Haryana",
      "HP|Himachal Pradesh",
      "JK|Jammu and Kashmir",
      "JH|Jharkhand",
      "KA|Karnataka",
      "KL|Kerala",
      "MP|Madhya Pradesh",
      "MH|Maharashtra",
      "MN|Manipur",
      "ML|Meghalaya",
      "MZ|Mizoram",
      "NL|Nagaland",
     " OR|Odisha",
     " PB|Punjab",
      "RJ|Rajasthan",
      "SK|Sikkim",
      "TN|Tamil Nadu",
      "TG|Telangana",
      "TR|Tripura",
      "UT|Uttarakhand",
      "UP|Uttar Pradesh",
      "WB|West Bengal",
      "AN|Andaman and Nicobar Island",
      "CH|Chandigarh",
      "DN|Dadra and Nagar Haveli",
      "DD|Daman and Diu",
      "DL|Delhi",
      "LD|Lakshadweep",
      "PY|Puducherry"]
 }
 this.myChangeHandler = this.myChangeHandler.bind(this)
 this.handlePasswordChange = this.handlePasswordChange.bind(this);
 this.toggleShow = this.toggleShow.bind(this);
  }
 

  handlePasswordChange(e) {
   
    this.setState({ password: e.target.value });
  }

  toggleShow(e) {
    e.preventDefault();
    this.setState({ hidden: !this.state.hidden });
  }
  componentDidMount() {
    if (this.props.password) {
      this.setState({ password: this.props.password });
    }
  }
  
 
  myChangeHandler = (evt) =>   
    {
      
      this.setState({ [evt.target.name]: evt.target.value });
   
    
  };
  onSubmit=(event)=> {
    
    alert("Hello " +this.state.username+
    "\nYour age :"+this.state.age+
    "\nYour gender :"+this.state.gender+
    "\nYour Birth Date :"+this.state.bdate+
    "\n Address:-"+this.state.Addtress+
    "\nYour State :"+this.state.state+
    "\n You have succefully Registered  ");
  }
 
       render() {
         
let p='';
if(this.state.username )
{
  p=<h2 style={{color: 'black'}} >Welcome {this.state.username} </h2>
}

    return (
      
      <div class="user" style={{backgroundImage:"abc.jpg"}}>
        
      <form  >
        
      {p}
        <p>Enter your name:</p>
        
        <input  placeholder="enter name here" 
          type="text" name="username" onChange={this.myChangeHandler}
        />
         <p>Enter your Password: </p>
         <input placeholder="enter password here" 
          type={this.state.hidden ? "password" : "text"}
          value={this.state.password}
          name="password" 
           onChange={this.handlePasswordChange}
        />
          {/* <input
          type={this.state.hidden ? "password" : "text"}
          value={this.state.password}
          onChange={this.handlePasswordChange}
        /> */}
        <button  onClick={this.toggleShow}>Show / Hide</button>
         <br></br>
        
          <label for="gender">Choose Your Gender
          <br></br>
         <select name="gender"  placeholder="select your gender" onChange={this.myChangeHandler}
         > 
          {this.state.lists1.map(list =>(
          <option key={list} value={list}>
            {list}
          </option>
          ))}
        {/* //  <option value="Male">Male</option>
        //      <option value="Female">Female</option>
        //   <option value="Other">Other</option> */}
           </select>
           </label>
           <br></br>
           <p>Enter your age:</p>
            <input  placeholder="enter age here" type="number" name="age" onChange={this.myChangeHandler} />
           <p>Enter your Date Of Birth:</p>
            <input  placeholder="DD/MM/YY" type="date"name="bdate" onChange={this.myChangeHandler} />
            <p>Enter your Address:</p>
            <textarea  name="Address" onChange={this.myChangeHandler}/>
        <p>Choose Your State:</p>
          <label for="State">
          <select  placeholder="select your state" name="state"  onChange={this.myChangeHandler}
         >{this.state.lists2.map(list =>(
          <option key={list} value={list}>
            {list}
          </option>
          ))}
              {/* <option value="Madhya Pradesh">Madhya Pradesh</option>
              <option value="Gujrat">Gujrat</option>
              <option value="Hariyana">Hariyana</option>
               <option value="Maharastra">Maharastra</option>
               <option value="Andhra Pradesh">Andhra Pradesh</option>
                <option value="Jammu Kashmir">Jammu Kashmir</option> */}
           </select>
           </label>
           <br></br>
          
  <button onClick={this.onSubmit} >Submit</button>
      </form>
      </div>
    );
  }

}
export default Farm;